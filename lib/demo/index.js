"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventTracker_1 = require("../EventTracker");
var params = {
    category: 'category1',
    action: 'action1',
    label: 'label1',
};
EventTracker_1.initTracker('https://httpbin.org/post');
EventTracker_1.tracker('send', 'event', params);
