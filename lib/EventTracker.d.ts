import { Tracker } from './Tracker';
export declare class EventTracker {
    private api;
    options?: Tracker.TrackerData;
    constructor(api: string);
    send?(fieldsObject: Tracker.TrackerData): Promise<void>;
}
export declare const initTracker: (api: string) => void;
export declare const tracker: Tracker.tracker;
