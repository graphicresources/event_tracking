"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventTracker_1 = require("./EventTracker");
exports.initTracker = EventTracker_1.initTracker;
exports.tracker = EventTracker_1.tracker;
