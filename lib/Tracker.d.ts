export declare namespace Tracker {
    interface TrackerData {
        category?: string;
        action?: string;
        label?: string;
        value?: number;
        [key: string]: any;
    }
    interface tracker {
        (command: 'send', hitType: 'event', fieldsObject: TrackerData): void;
    }
}
