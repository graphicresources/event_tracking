import { initTracker, tracker } from '../EventTracker';
const params = {
    category: 'category1',
    action: 'action1',
    label: 'label1',
};
initTracker('https://httpbin.org/post');
tracker('send', 'event', params);