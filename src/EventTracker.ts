import Axios from 'axios';
import * as FormData from 'form-data';
import { Tracker } from './Tracker';

export class EventTracker {
    private api: string = '';
    private headers = {
        'Content-Type': 'multipart/form-data',
    }
    private callbackCSRF?: Function;
    public options?: Tracker.TrackerData;

    public setSetting(settings?: string | EventTrackerSettings) {
        if (typeof settings === 'string') {
            this.api = settings;
        } else if (settings) {
            if (settings.api) this.api = settings.api;
            if (settings.headers) this.setHeaders(settings.headers);
            if (settings.callbackCSRF) this.callbackCSRF = settings.callbackCSRF;
        }
    }

    public setHeaders(headers: {[name: string]: any}) {
        this.headers = { ...this.headers, ...headers};
    }

    public async send(fieldsObject: Tracker.TrackerData) {
        const track = this;
        track.options = fieldsObject;

        var formData = new FormData();
        for (var key in track.options) {
            formData.append(key, track.options[key]);
        }

        try {
            Axios.post(
                track.api,
                formData,
                {
                    headers: {
                        ...this.headers
                    }
                }
            ).then(response => {
                const { data } = response;
                this.updateCSRF(data);
            }).catch(error => {
                const { data } = error.response;
                this.updateCSRF(data);
            });
        } catch (error) {}
    }

    private updateCSRF(data: any) {
        if (this.callbackCSRF && data.csrfToken) {
            this.callbackCSRF(data.csrfToken);
        }
    }
}

const eventTracker = new EventTracker();

export const initTracker = (settings: string | EventTrackerSettings) => {
    eventTracker.setSetting(settings);
}

export const tracker: Tracker.tracker = (...args: any[]): void => {
    if (args.length > 1 && args[0] === 'send' && args[1] === 'event') {
        eventTracker.send(args[2]);
    }
}

export const setHeadersTracker = (headers: {[name: string]: any}) => {
    eventTracker.setHeaders(headers);
}

interface EventTrackerSettings {
    api: string;
    headers?: {
        [name: string]: any
    };
    callbackCSRF?: Function;
}
